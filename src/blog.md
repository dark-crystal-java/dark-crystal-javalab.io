# Blog - Designing a social backup feature for Briar

- [Briar and Dark Crystal Meetup - Introduction](./blog/briar-dc-meetup.md)
- [1st Iteration - Initial White-boarding](./blog/first-iteration-workflow.md)
- [2nd Iteration - Improving workflow](./blog/Second-iteration-of-briar-distributed-account-backup-workflow.md)
- [3rd Iteration - Refining the design](./blog/third-iteration-workflow.md)
- [4th Iteration - Finalising workflow](./blog/4th-iteration-workflow.md)
- [5th Iteration - UI mock-up on Android](./blog/5th-iteration-workflow.md)
- [6th Iteration - Back-end flowcharts](./blog/6th-iteration-workflow.md)
- [7th Iteration - UI terminology](./blog/7th-iteration-workflow.md)
