## References

- Allen, C & Friedenbach, M. "A new approach to social Key Recovery" https://github.com/WebOfTrustInfo/rwot8-barcelona/blob/master/topics-and-advance-readings/social-key-recovery.md
- Beimel, Amos (2011). "Secret-Sharing Schemes: A Survey" http://www.cs.bgu.ac.il/~beimel/Papers/Survey.pdf
- Blakley, G.R. (1979). "Safeguarding Cryptographic Keys". Managing Requirements Knowledge, International Workshop on (AFIPS). 48: 313–317. doi:10.1109-/AFIPS.1979.98.
- Feldman, Paul (1987) "A practical scheme for non-interactive Verifiable Secret Sharing" Proceedings of the 28th Annual Symposium on Foundations of Computer Science
- Pedersen T.P. (1992) "Non-Interactive and Information-Theoretic Secure Verifiable Secret Sharing". In: Feigenbaum J. (eds) Advances in Cryptology — CRYPTO ’91. CRYPTO 1991. Lecture Notes in Computer Science, vol 576. Springer, Berlin, Heidelberg
- Shamir, Adi (1979). "How to share a secret". Communications of the ACM. 22 (11): 612–613. doi:10.1145/359168.359176.
- SLIP39 Specification  https://github.com/satoshilabs/slips/blob/master/slip-0039.md
- Sprenkles, Daan 'We Should Share our Secrets' - Talk at 34c3 https://media.ccc.de/v/34c3-8885-we_should_share_our_secrets 
