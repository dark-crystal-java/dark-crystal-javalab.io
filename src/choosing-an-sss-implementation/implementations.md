## Implementations considered
 
### Pure Java implementations considered:

As the others parts of the reference implementation of the Dark Crystal distributed key backup system will be written in Java, we considered some pure java implementations.  We were unable to find one which met our requirements, mostly due to them not being actively maintained. 

- https://github.com/codahale/shamir uses 256 bit Galois field. Has been archived by the author, implying it is no longer actively maintained
- https://github.com/MatthewDavidBradshaw/Shamir Comes bundled together with a graphical user interface, which we don't need. Last commit in 2018.
- https://github.com/secretsharing/secretsharing/ Last commit 2017

### C implementations:

This would be our ideal choice, since it is possible to have 'bindings' written in popular high level languages, giving totally consistent behaviour across implementations, making the shares interoperable, and eliminating the need to monitor multiple libraries for security flaws or updates.

- Daan Sprenkles' 'sss' is a C library with bindings already written for NodeJS, Go, Rust, and Webassembly. The documentation includes a table comparing the security features of the library with 8 other popular implementations.  In particular, protection against attacks using side-channel analysis (such as timing) is included. Although in some cases protection against such attacks might not be necessary, this indicates the author's rigorous approach to security. A 'submodule' containing a secure random number generator is also included. Members of our team have met the author personally and discussed security, and his talk at the Chaos Communications Congress (see references) about his library outlines the security considerations he has made. The library has been reviewed by other cryptographic engineers at Radboud University, Nijmegen. Project started in 2017, latest commit September 2019. https://github.com/dsprenkels/sss

- B. Poettering's 'ssss' is much older (created in 2005), and is widely adopted, packaged as a command line interface which is available in many linux distributions including debian. However it is (as far as we know) no longer maintained, and does not give an authentication method to make shares 'tamper resistant'.

### SLIP39 based:

- 'BlockchainCommons/sss' is a fork of the C library 'dsprenkles/sss' (described below), modified to be SLIP39 compliant.  It is no longer maintained due to being 'superceded' by the library below. https://github.com/BlockchainCommons/sss
- 'BlockchainCommons/BC-SLIP39' is another C implementation of SLIP39. It is a complete re-write of Sprenkles' library. It is actively developed but we feel it is not mature enough - the initial commit was made in March 2020. https://github.com/BlockchainCommons/bc-slip39

