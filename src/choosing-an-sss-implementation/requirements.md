## Requirements of the implementation

* Compact share size. In some cases it is important that shares are not too long, for example when they are going to be encoded as QR codes, or when many shares are to be stored on a mobile device.  So using efficient encoding and only including the necessary metadata is important.

* Using a secure random number generator with a good source of entropy, or allowing either a custom random number generator or an additional source of entropy to be passed in (in some cases it can be useful to take additional entropy from the secret itself, assuming the secret is uniformly random).

* Flexible. We are looking for a low-level library, giving only basic functionality and allowing extra security features to be added depending on the use-case. In practice, many projects will need a message authentication code (MAC), padding to obfuscate secret length, and obfuscation of the 'share id' (x coordinate).  Many implementations come with some of these features, but we want developers to be able to choose what best suits their project.

* Actively maintained. While there are unlikely to be many fixes, it is important that the library is maintained and the maintainers are contactable should security issues arise.

* Mature, reviewed, and adopted. Being adopted by other projects, and having had time for problems to emerge and be resolved, are good signs.  Independent reviews are also important.

