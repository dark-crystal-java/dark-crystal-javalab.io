## Conclusion

Daan Sprenkles 'sss' C library was chosen, for its security features, active maintenance, adoption, existing bindings to high level languages, and the author's academic background in cryptographic engineering.

Although there exists an unfinished 'work-in-progress' project to create Java bindings to 'sss' for Android, it uses JNI.  We have chosen to create our own using JNA as the procedure is much simpler.

