## Bindings to Java

There are two popular systems for writing bindings to native code in Java. [Java Native Access (JNA)](https://github.com/java-native-access/jna) and [Java Native Interface (JNI)](https://docs.oracle.com/javase/8/docs/technotes/guides/jni/). 

Both systems work on android. But JNA bindings require less work, and are easier to maintain.

