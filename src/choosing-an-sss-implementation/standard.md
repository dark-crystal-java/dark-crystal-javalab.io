## A standard for Shamir's secret sharing

The only standardised protocol for implementing such secret sharing schemes that we are aware of is 'Satoshi Labs Improvement Proposal 39' (SLIP39), entitled 'Shamir's Secret-Sharing for Mnemonic Codes'.  Although this is an open-source standard, it is written and maintained by blockchain company 'Satoshi Labs', who also produce the hardware cryptocurrency wallet, 'Trezor'.

The scheme is not said to be specifically for use with keys for cryptocurrency, but simply 'describes a standard and interoperable implementation of Shamir's secret-sharing'.  However, the proposal does refer several times to Bitcoin wallets and it is clear that addressing this use-case is a priority, which we feel is very different from the use-case we are proposing for Dark Crystal.

However, the specification does include several noteworthy security features, and there are some promising implementations based on it.

For example, most implementations we looked at take the secret to be the `y`-value of point `x=0` on the polynomial. SLIP39 suggests constructing the polynomial such that `x=254` is the secret and `x=255` is the hash of the secret. This gives authentication - we have a way of knowing that the secret was successfully recovered. Another approach to this is to use an encrypted secret with a MAC.

SLIP39 also introduces a 'two tier' scheme whereby there are different kinds of custodians with different levels of trust assigned to them, as is described in the article 'A New Approach to Social Key Recovery'.  For example, there might be a 'high trust' group of family members and a 'lower trust' group of colleagues.  As well as assigning different levels of trust, this also has the advantage that requiring a combination of shares from different groups, might reduce the likelihood of custodians colluding against the secret owner.

Although the 'two tier' system has clear advantages, our concern is that it further complicates what is already a new and complex concept for users, and also that it could optionally be implemented at a higher level.  We are looking for a standard which is simple and unopinionated, allowing extra functionality to be added depending on the use-case.

For this reason, as well as the orientation towards cryptocurrency use-cases, we decided not to adopt and comply with SLIP39, but to take inspiration from the security features it offers when considering higher-level features of our protocol.

