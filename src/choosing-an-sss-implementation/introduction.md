## Introduction

The difficulty with threshold-based secret sharing, compared to other cryptographic operations is that there is no widely accepted, rigorously defined standard.

Shamir's paper describing the technique was published 1979 (with a similiar technique proposed by Blakley the same year), and since then there has been a great amount of academic work building and improving on it. Much of this work focusses on introducing verification of shares, such as the schemes proposed by Feldman and Pederson. However, turning a proven mathematical scheme into a secure implementation in code is rarely straightforward. Often design decisions need to be made which might deviate from the specification or introduce flaws or complications.

We aim to create a protocol which can be adopted by a wide variety of projects, but reliably gives consistent behaviour.  So it is important to be agnostic to the choice of programming language. This means either proposing a rigorously defined standard, citing a clearly documented 'reference implementation' which can be re-implemented in other languages, or choosing an implementation in a low-level language which can be called by popular high-level languages through 'bindings'.

