
<<<<<<< HEAD
=======

>>>>>>> 818bf1bfda31ef0e982eda08a7f3e5a586785e11
# Why Dark Crystal?

Dark Crystal is a protocol for distributed data persistence and threshold-based consensus. It is based on a secure implementation of Shamir’s Secret Sharing and has multiple possible applications in security-oriented tools. 

Modern encryption techniques are strong, but rarely used by those who need them. A recurring reason for this is users’ fears of losing access to critical data: the ‘Global Encryption Trends Study 2018’, conducted by the Ponemon Institute, indicates that key management issues pose a major barrier to the adoption of encryption tools. 

Mechanisms such as privately owned offline storage in a secure location, virtual private servers, a trustworthy and reliable cloud service, or the data storage infrastructure provided by certain NGOs, have their own intrinsic limitations. These methods require that users be conceptually comfortable with digital security issues and familiar with key management practices, such that they can back-up their keys (or data) independently of the related application.

Moreover, while traditional forms of 'secure backup' make sense for sensitive media such as incriminating photos, they are less suitable for personal cryptographic keys. In the case of signing keys for creating verifiable evidence, it would undermine the strength of this evidence if another party took complete custody of the key. In the case of encryption keys for personal messages, insecure or unencrypted backups can create a weak point in security. There are also security risks involved with transmitting cryptographic keys over the internet.

Given the vulnerability of centralised systems - demonstrated by the experiences of Telegram and Matrix - we believe that offering users at least the option of distributed secure backups - and therefore the ability to wipe their device when needed - can at the very least provide a complementary alternative option for developers and users of encrypted tools. 

Finally, as peer-to-peer protocols advance in response to widespread security concerns with centralised client-server architectures, key (and/or data) backup becomes an even more serious issue. Developing distributed backup and remote wipe features for peer-to-peer applications, to match features already available in client-server architectures, gives both developers and users a greater and more robust set of tools to choose from to address their particular digital security needs.

In general, Shamir's scheme is considered information-theoretically secure. That is, individual shares contain absolutely no semantic information about the secret, and it can be said to be 'post quantum' cryptography. As an interesting anecdote, the root key for ICANN DNS security, effectively the key which secures the naming system of the internet, is held by seven parties, based in Britain, the U.S., Burkina Faso, Trinidad and Tobago, Canada, China, and the Czech Republic. Cryptographer Bruce Schneier has alleged that they are holders of Shamir's secret shares, which indicates that the scheme is taken quite seriously.







<<<<<<< HEAD
=======
<!-- 
TODO: fill out this narrative


typical client-server architecture is..

it allows the following things...

but it has the following problems...

privacy and security focused apps are increasingly turning to p2p architecture to solve this

this has the following benefits...
  environmental

but also the following problems...

dark crystal has the potential to solve these problems, providing p2p apps the ability to offer features that this architecture did previously preclude

moreover, it can endow p2p with a solution to the password problem that client-server apps have still not satisfactorily resolved


Key management has repeatedly been highlighted as a major factor limiting uptake of encrypted communication tools. 

this has been identified in needfinding research across the world

it is also an issue with the rise of p2p tools

p2p tools offer some benefits over client-server architecture, and in many situations are more appropriate for use

but they also rely on strong encryption, and losing one's key can mean losing large amounts of data, one's entire contact list, one's online identity and one's account.

moreover, alongside clear benefits, there are some features that p2p makes hard - data recovery and remote wipe being two big ones.

sharding can help to solve these problems.

Dark Crystal is a set of protocols and recommendations which aim to address the key management problem, encourage good practice when designing peer-to-peer applications, and make it safer and easier for non-technical peoples to use encrypted communication tools.

We are confident that provided our mechanism is well integrated into the application and has a good user interface, this process can be made very easy.

sharding has the potential to offer peer-to-peer apps the following capabilities, typically precluded by their serverless architecture.

1. Key Management: Modern encryption techniques are strong, but rarely used by those who need them. A recurring reason for this is users’ fears of losing access to critical data.

2. Data Recovery: High risk users wish to ‘clean’ particular apps from their device in emergencies but do not want to lose their data permanently.

3. Distributed Remote Wipe: Due to risks of both pickpocketing and arrest, journalists and activists want the ability to remotely delete data from their device.

Our original prototype is a stand alone, generalised tool which can be used to backup keys from different applications. Therefore users must have an understanding of the purpose and importance of cryptographic keys and manually import them into the application. We identified this as a major usability pain-point and so want to fully integrate the mechanism together with Briar to achieve an intuitive user-experience that does not require users to understand the complex underlying technologies.

final paragraph tying it all together.


----------------------------
old texts to pull from:

Moreover, the rise of peer-to-peer systems, while providing many benefits over client-server architecture, relies on strong encryption and faces the same issue. 

Needfinding research across communities from Vietnam, Uganda, Ukraine, Tunisia and Tibet have identified a number of unaddressed concerns shared by activists and journalists working across diverse contexts and with diverse digital tools. These include:




In recent years the limitations of the client-server model have become ever more apparent. The server operators are in a very powerful position. They are able to:

* Discontinue the service.
* Increase costs or change the terms of the service.
* Revoke access to particular users or groups of users.
* Include hidden functionality.
* Extract or commodify user's personal data.

At the same time, peer-to-peer systems have become more advanced, overcoming many of the limitations they once had. For example:

A peer can store data belonging to other peers, without being able to access it themselves.

A peer can verify the integrity of a given data set, and verify whether it was written by a particular author, and that the set is complete up to the most recent item given.

Peer-to-peer systems are fundamentally empowering to those that use them, their very nature makes it difficult for a minority to control or exploit them. Provided the software they run is open source and community developed, they offer a much more democratic way of working.

But having ultimate control over your own data comes at a cost. Without a trusted party providing authentication, peers have to manage cryptographic keys. Losing them not only means you lock yourself out, but someone who finds them could impersonate you.
 -->



>>>>>>> 818bf1bfda31ef0e982eda08a7f3e5a586785e11
<!-- 
TODO: fill out this narrative


typical client-server architecture is..

it allows the following things...

but it has the following problems...

privacy and security focused apps are increasingly turning to p2p architecture to solve this

this has the following benefits...
  environmental

but also the following problems...

dark crystal has the potential to solve these problems, providing p2p apps the ability to offer features that this architecture did previously preclude

moreover, it can endow p2p with a solution to the password problem that client-server apps have still not satisfactorily resolved


Key management has repeatedly been highlighted as a major factor limiting uptake of encrypted communication tools. 

this has been identified in needfinding research across the world

it is also an issue with the rise of p2p tools

p2p tools offer some benefits over client-server architecture, and in many situations are more appropriate for use

but they also rely on strong encryption, and losing one's key can mean losing large amounts of data, one's entire contact list, one's online identity and one's account.

moreover, alongside clear benefits, there are some features that p2p makes hard - data recovery and remote wipe being two big ones.

sharding can help to solve these problems.

Dark Crystal is a set of protocols and recommendations which aim to address the key management problem, encourage good practice when designing peer-to-peer applications, and make it safer and easier for non-technical peoples to use encrypted communication tools.

We are confident that provided our mechanism is well integrated into the application and has a good user interface, this process can be made very easy.

sharding has the potential to offer peer-to-peer apps the following capabilities, typically precluded by their serverless architecture.

1. Key Management: Modern encryption techniques are strong, but rarely used by those who need them. A recurring reason for this is users’ fears of losing access to critical data.

2. Data Recovery: High risk users wish to ‘clean’ particular apps from their device in emergencies but do not want to lose their data permanently.

3. Distributed Remote Wipe: Due to risks of both pickpocketing and arrest, journalists and activists want the ability to remotely delete data from their device.

Our original prototype is a stand alone, generalised tool which can be used to backup keys from different applications. Therefore users must have an understanding of the purpose and importance of cryptographic keys and manually import them into the application. We identified this as a major usability pain-point and so want to fully integrate the mechanism together with Briar to achieve an intuitive user-experience that does not require users to understand the complex underlying technologies.

final paragraph tying it all together.


----------------------------
old texts to pull from:

Moreover, the rise of peer-to-peer systems, while providing many benefits over client-server architecture, relies on strong encryption and faces the same issue. 

Needfinding research across communities from Vietnam, Uganda, Ukraine, Tunisia and Tibet have identified a number of unaddressed concerns shared by activists and journalists working across diverse contexts and with diverse digital tools. These include:




In recent years the limitations of the client-server model have become ever more apparent. The server operators are in a very powerful position. They are able to:

* Discontinue the service.
* Increase costs or change the terms of the service.
* Revoke access to particular users or groups of users.
* Include hidden functionality.
* Extract or commodify user's personal data.

At the same time, peer-to-peer systems have become more advanced, overcoming many of the limitations they once had. For example:

A peer can store data belonging to other peers, without being able to access it themselves.

A peer can verify the integrity of a given data set, and verify whether it was written by a particular author, and that the set is complete up to the most recent item given.

Peer-to-peer systems are fundamentally empowering to those that use them, their very nature makes it difficult for a minority to control or exploit them. Provided the software they run is open source and community developed, they offer a much more democratic way of working.

But having ultimate control over your own data comes at a cost. Without a trusted party providing authentication, peers have to manage cryptographic keys. Losing them not only means you lock yourself out, but someone who finds them could impersonate you.
 -->
