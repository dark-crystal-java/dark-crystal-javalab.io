## Descriptions of modules used in the reference implementation

- [Reference implementation API Documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/)

### [dark-crystal-shamir-secret-sharing](https://gitlab.com/dark-crystal-java/dark-crystal-shamir-secret-sharing)

The secret sharing algorithm. This provides JNA bindings to the C library [dsprenkels/sss](https://github.com/dsprenkels/sss).

### [dark-crystal-secret-sharing-wrapper](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper)

A wrapper around libSSS providing higher functionality specific to dark-crystal key backup, including:
- Variable length secrets with authentication
- Obfuscations of the x-coordinate (discussed below/above*)
- Optional zero-padding

### [dark-crystal-key-backup-crypto-java](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java) 

Contains cryptographic operations used for key backup and recovery.

### [dark-crystal-key-backup-message-schemas-java](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java)

Contains template schemas for Dark Crystal key backup messages, using protocol buffers, XML, and JSON.
