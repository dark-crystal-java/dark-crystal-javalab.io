## Anatomy of a shard

A shard consists of the share component (a share of the key to the secret), the encrypted secret component, and a signature.  Including the share index, share, nonce, MAC, and signature mean the share length is 137 bytes, plus the length of the secret, or a standard length if zero-padding is used.

![Anatomy of a shard](../assets/anatomy-shard.png)

