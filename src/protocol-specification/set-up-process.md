## Setup process

This section walks through the process making a distributed backup of a secret

### Step 1 - Secret is combined with contextual metadata

![secret and label](../assets/dc_secret_label.png)

This step is optional and is to indicate the intended purpose of the secret, meaning that it is still useful if recovered 'out of context'. This means including a 'label' property which will be a human readable description, including, for example, the name of the application it is useful for. This may also include application-specific data.

The amount of information included here depends on how critical it is that share size is kept small.

- [`SecretSharingWrapper.SecretWithLabel` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.SecretWithLabel.html)
- [`SecretSharingWrapper.decodeSecretWithLabel` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#decodeSecretWithLabel(byte%5B%5D))

### Step 2 - Encrypt data with symmetric key and Message Authentication Code added

![secret and label with key](../assets/dc_secret_label2.png)

The data is encrypted with a symmetric key and this key is taken to be the secret. Otherwise, the data itself is taken to be the secret. It needs to be noted that many implementations of secret sharing do this internally, and produce shares which are a concatonation of a key-share and the encrypted secret.

This means there is some duplication of data - a portion of each share is identical to the others. So in the case of particularly large secrets, it makes sense if the encrypted secret is stored only once in a place which is accessible to all share-holders (if the practicalities of the chosen transport/storage layer make this possible).

![secret label key mac](../assets/dc_secret_label3.png)

A `poly1305` MAC is used, which allows us to later verify that the secret has been correctly recovered.

- [`KeyBackupCrypto.secretBox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#secretBox(byte%5B%5D,byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.secretBox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L191)

### Step 3 - Shards generated

![shards](../assets/dc_shards1.png)

Shards are generated using a secure threshold-based secret sharing algorithm, [dsprenkels/sss](https://github.com/dsprenkels/sss) 

### Step 4 - Shards are signed

Each shard is signed by the owner of the secret using a keypair with an established public key (such as the same keypair used to sign other messages in the application).

Shards are appended with these signatures, meaning the complete shard format looks like this:

![shard format](../assets/shard-format.png)

This is to protect against shards being modified, maliciously or accidentally, and achieves the same goal as schemes known as 'Verifiable Secret Sharing'.

This is discussed in detail in our threat model report.

- [`SecretSharingWrapper.shareAndSign` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#shareAndSign(byte%5B%5D,int,int,java.security.PrivateKey))
- [`SecretSharingWrapper.shareAndSign` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L164)
- [`SecretSharingWrapper.verifyAndCombine` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#verifyAndCombine(java.util.List,java.security.PublicKey))
- [`SecretSharingWrapper.VerifyAndCombine` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L183)

### Step 5 - Schema version number and timestamp added

A version number ensures backward compatibility with future versions of the protocol.

A timestamp allows secret-owner and custodian to keep track of what happened when.

- [Example of a timestamp being added to a message in the source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java/-/blob/297c9417b4e08adee11ee6985e66509d7b7b55a5/src/main/java/org/magmacollective/darkcrystal/keybackup/messageschemas/Publish.java#L50)
- [Example of a version number being added to a message in the source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java/-/blob/297c9417b4e08adee11ee6985e66509d7b7b55a5/src/main/java/org/magmacollective/darkcrystal/keybackup/messageschemas/Publish.java#L48)

### Step 6 - Signed shards encrypted for each custodian

![shards](../assets/dc_shards2.png)

Shards are encrypted with the public key of each custodian. The unencrypted shards are removed from memory. 

'One-way box' is used to ensure the custodian can read the message, but the secret-owner cannot read it themselves.

This is inspired by [private-box](https://github.com/auditdrivencrypto/private-box), see [this note in the design document](https://github.com/auditdrivencrypto/private-box/blob/master/design.md#one-way-box).  We could also just use private-box itself for this, but this method is simpler and gives us smaller shards.

- [`KeyBackupCrypto.oneWayBox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayBox(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.oneWayBox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L272)
### Step 7 - Transmission

Each encrypted shard is packed together with some metadata into a message, transmitted to the custodian, and a local (encrypted) copy is retained.

Additionally, a '`root`' message is created which contains some metadata describing the secret. This message is retained locally and serves as a record of the backup.

Details of these messages, as well as of the system of requesting, responding, and forwarding shards, are explained in the [message schemas section](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java)

- [Message Schemas API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-message-schemas/org/magmacollective/darkcrystal/keybackup/messageschemas/package-summary.html)
- [Message schemas source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-message-schemas-java)

