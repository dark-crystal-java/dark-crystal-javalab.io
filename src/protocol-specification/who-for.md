## Who is this for?

This protocol is designed to be integrated into applications which deal with sensitive data or where effective key management is critical.

The protocol is agnostic to transport and storage mechanisms, meaning messages used for this backup system can be transmitted and stored in the same way your application handles other types of data. The benefit of this is that it should be easy to integrate, and not require too many additional dependencies.

However, when deciding whether this protocol is a good fit for your application, several factors need to be taken into account. These are discussed in more detailed in our 'Threat modelling' and 'Social factors' reports.

The architecture of the application has an effect on how effective these techniques are.  This backup technique is robust because of its distributed nature. Ideally, shards are stored in multiple locations controlled by multiple 'custodians'. If the application uses a traditional client-server architecture, there is a concern that all shards are stored on the same server, meaning the same physical location.

This is perhaps be not as bad as it sounds, since each shard would be encrypted to a particular custodian, and the custodian's secret key should be stored only on their client device. But it is far from ideal, as a compromised server would mean a metadata leak as well as a possibility that the shards could be lost.

So while it is possible, though not advised, to use a centralised server to coordinate communication between peers, it must be emphasised that shards must be stored locally on client devices.  Reliance on a particular server is analogous to "putting all your eggs in one basket", and detracts from the core principle of this protocol, which is to provide a distributed backup. 

## Peer experience

This is a technical specification of the protocol. Many stages of the process will be automated and not visible to the peers.  Interface recommendations and peer stories will be described elsewhere.
