## Introduction

Dark Crystal is a social key management system.  It is a set of protocols and recommendations for responsibly handling sensitive data such as secret keys. 

It is designed for safeguarding data which we don't want to loose, but which we don't want others to find. 

The idea is that rather than creating a generalised piece of software for managing keys, key management techniques should be integrated into the applications which use the keys. So the techniques described and libraries provided can be seen as recommendations for developers wanting to improve key management for particular applications.  Of course, every situation is different and in many cases the protocol will need to be adapted for particular needs.

This document describes a social key backup and recovery technique, to enable lost keys, passwords or other sensitive data to be recovered, using a small group of trusted contacts.

It must be emphasised that key recovery cannot solve the problem of compromised keys. It is appropriate only to recover encrypted data following loss of a key, or for continued use of a key when it is known to be lost but not compromised, for example following accidental deletion or hardware failure. 

## Terms used

- ***Secret*** - the data to be backed up and potentially recovered.
- ***Secret-owner*** - the peer to whom the data belongs and who initiates the backup.
- ***Shard*** - a single encrypted share of the secret. 
- ***Custodian*** - a peer who holds a shard, generally a friend or trusted contact of the secret owner.

## Key Loss Scenarios

These scenarios will be discussed in greater depth in the report on social factors. But it is important to already make the distinction between the three main kinds of key loss:

- ***'Swim'*** - key loss - For example, the computer fell into the sea and is lost forever.  In the case the key is assumed lost, but not compromised.
- ***'Theft'*** - key compromise - For example, I forgot my computer on a train and have no idea who might have it.  The key is both lost and compromised.
- ***'Inheritance'*** - following death, incapacitation or imprisonment, a key can be recovered by heirs.  This covers any situation where custodians recover the secret without the involvement of the secret owner.

