## Cryptographic primitives

### Introduction

Standards are generally adopted from the [Networking and Cryptography library (NaCl)](https://nacl.cr.yp.to/). These were chosen because they are widely adopted and regarded to be secure, and available in a wide variety of high level languages, either through bindings to the C library ['libsodium'](https://doc.libsodium.org/) (which is based on NaCl), or by high level implementations which are based on the same cryptographic parameters and algorithms and can reproduce the same behaviour in tests.

### Notation used

![concatonation](../assets/concatonation.png)

Concatonation of a and b

![scalar multiplication](../assets/scalar_multiplication.png)

Deriving a shared key from keys a and b by scalar multiplication 

![box](../assets/box.png)

Authenticated encryption of message with key

![HMAC](../assets/hmac.png)

Keyed hash of message 

### Authenticated Symmetric Encryption

The symmetric encryption algorithm used is NaCl's 'secretbox', which consists of the `XSalsa20` stream cipher, and a `poly1305` message authentication code, both developed by Daniel J. Birnstein.  These were chosen because they are widely adopted and regarded to be secure.

We encode encrypted messages as the nonce concatonated with the MAC, concatonated with the ciphertext:

![nonce MAC ciphertext](../assets/nonce_mac_ciphertext.png)

Since the nonce used by XSalsa20 is 24 bytes and the MAC used by poly1306 is 16 bytes, ciphertext messages are `plaintext length + 24 + 16` bytes long.

- [`KeyBackupCrypto.encrypt` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#encrypt(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.decrypt` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#decrypt(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.secretBox` (used internally by `encrypt`) in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L191)

#### External documentation links:

- [Daniel J. Bernstein 2005, Salsa20 Specification](https://cr.yp.to/snuffle/spec.pdf)
- [Daniel J. Bernstein, "Cryptography in NaCl" - formal specification](https://cr.yp.to/highspeed/naclcrypto-20090310.pdf)
- [nacl secretbox documentation](http://nacl.cr.yp.to/secretbox.html)
- [libsodium secretbox documentation](https://download.libsodium.org/doc/secret-key_cryptography/secretbox) - libsodium is a compatible fork of NaCl, with extended functionality.

### Signing and Verification

Signing is achieved using the Edwards curve digital signature algorithm (EdDSA), using recommended parameters for the Ed25519 and Ed448 elliptic curves.

- [EdDSA in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.html)
- [EdDSA in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.java)

#### External documentation links:

- [Edwards-Curve Digital Signature Algorithm (EdDSA) - IETF RFC8032](https://tools.ietf.org/html/rfc8032)
- [ed25519 23pp. Daniel J. Bernstein, Niels Duif, Tanja Lange, Peter Schwabe, Bo-Yin Yang. High-speed high-security signatures. Journal of Cryptographic Engineering 2 (2012), 77–89.](https://cr.yp.to/papers.html#ed25519)
- [NaCl's `crypto_sign/ed25519`](https://ed25519.cr.yp.to/software.html)
- Java implementation used: [net.i2p.crypto:eddsa](https://mvnrepository.com/artifact/net.i2p.crypto/eddsa/0.3.0) [ed25519-java github repository](https://github.com/str4d/ed25519-java) - this has undergone two independent reviews, and the unit tests include tests against data from the python implementation.

### Public Key Encryption

An agreement (shared secret) is calculated using Curve25519 keypairs. Both public and private keys are 32 bytes long.

Because of the way scalar multiplication works, the space of possible DH agreements is smaller than the space of possible keys, that is to say, there exist several sets of public and private keys which will give the same DH agreement.  To address this problem, we 'hash in' both parties' public keys to increase the 'uniqueness' of the shared secret.

Optionally, additional contextual information known by both parties can also be concatonated to the public keys and 'hashed in' to the secret, providing additional forward secrecy.

The shared secret is calculated by both parties as:

![shared secret](../assets/shared_secret.png)

Authenticated encryption is then achieved as described above, using the Xsalsa20 stream cipher with poly1305 MAC (secretbox).

- Java implementation of Curve25519 [org.whispersystems:curve25519](https://mvnrepository.com/artifact/org.whispersystems/curve25519-java/0.5.0) - this provides both a native implementation using JNI, and a pure Java 7 implementation.
- [`KeyBackupCrypto.box` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#box(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair,byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.unbox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#unbox(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair,byte%5B%5D,byte%5B%5D))
- [`KeyBacupCrypto.box` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L97)
- [`KeyBacupCrypto.unbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L127)

### Hashing

Hashing is done using a Blake2b keyed hash, with a digest length of 32 bytes unless specified otherwise.

- [`KeyBackupCrypto.blake2b` in API documentation (with key given)](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#blake2b(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.blake2b` in API documentation (without key)](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#blake2b(byte%5B%5D))
- [`KeyBackupCrypto.blake2b` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L68)

### Key derivation

Curve25519 (encryption) keys can be derived from Ed25519 (signing) keys, in such a way that both secret and public components can be derived, meaning we only need to establish a signing keypair.  This is very convenient as it reduces the amount of keys we need to manage, but it is regarded by many cryptographers as bad practice and is not recommended if it can be avoided.

