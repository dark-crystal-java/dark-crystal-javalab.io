## Higher level cryptographic functions

### 'One Way Box'

This is used to encrypt a message allowing a given recipient to decrypt it without knowing the identity of the sender. This is useful because it reduces the metadata required to be carried with the message, obfuscating the identity of the sender from an eavesdropper.

Furthermore, it is useful for its one-way property. After the sender has created the message, they are not able to decrypt it themselves.  This makes it particularly useful for encrypting shards, because if all the shard messages are retained on the device of the secret-owner after being sent out, it is important that they cannot be recovered by the secret-owner themselves, as this would otherwise comprise of an extra copy of the secret being stored in a single location.

For transport systems which use append-only logs, this is absolutely essential, as it is not possible to remove the sent messages.

It works by using an ephemeral keypair for the sender, rather than their long-term key which is normally used. The public key ephemeral key is included with the ciphertext, and the private key is discarded after being used a single time, and never stored on disk.

![one way box](../assets/one-way-box.png)

Since shards should always be signed with the long term signing key of the secret-owner, and are never transmitted without this signature, using an ephemeral key for encryption does not introduce any doubt as to the identity of the secret owner.

- [`KeyBackupCrypto.oneWayBox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayBox(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.oneWayUnbox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayUnbox(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair))
- [`KeyBackupCrypto.oneWayBox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L272)
- [`KeyBackupCrypto.oneWayUnbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L287)

