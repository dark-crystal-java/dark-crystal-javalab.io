## Recovery process

This section walks through the process of recovering a secret from a distributed backup

### Step 1 - New identity

Upon loss of data, the secret owner establishes a new account, giving them a new identity on the system. This step depends a lot on the transport mechanism used, but will generally involve generating a new keypair.

### Step 2 - Contact custodians

The secret owner contacts the custodians 'out of band' to confirm that the new identity belongs to them. That is, it is assumed that there is the possibility of some personal contact to convince the custodians that the new identity is really the secret owner.  For example this might involve a phone call saying "hey, its me!".

Due to the threshold nature of the scheme there is a degree of tolerance to some custodians being unavailable or uncooperative.

The user interface should be designed to encourage the custodians to get the secret owner to confirm their new key out of band.  If on a phone call, rather than trying to confirm some characters from the key itself, it can be much easier to confirm some dictionary words derived from the key.

### Step 3 - Return shards

Each custodian decrypts the shard they are holding with their personal keypair, and re-encrypts it to the public key of the new account. It is then sent to the new account of the secret owner.

The nature peer to peer protocols make it difficult to delete data. If this is the case with the transport mechanism you are using, we recommend adding a second layer of encryption using an ephemeral keypair. This is a single-use keypair which can later be deleted to effectively delete these messages from the system.  This will be explained in more detail in a separate document.

### Step 3 - Decrypt shards

The secret owner decrypts the shards they receive, using `oneWayUnbox` (described above).

- [`KeyBackupCrypto.oneWayUnbox` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#oneWayUnbox(byte%5B%5D,org.whispersystems.curve25519.Curve25519KeyPair))
- [`KeyBackupCrypto.oneWayUnbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L287)

### Step 4 - Validate shards

![validated shards](../assets/validated-shards-sm.png)

The signature of each shard is validated with the original public key, proving that the returned shards are identical to those sent out. 

In the case that the shard could not be validated, the shard data can be retrieved anyway using `detachMessage`.

- [`SecretSharingWrapper.VerifyAndCombine` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#verifyAndCombine(java.util.List,java.security.PublicKey))
- [`SecretSharingWrapper.VerifyAndCombine` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L183)
- [`KeyBackupCrypto.detachMessage` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.html#detachMessage(byte%5B%5D))
-[`KeyBackupCrypto.detachMessage` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/EdDSA.java#L69)

### Step 5 - Secret recovery

![recovery](../assets/recovery-sm.png)

The shards are combined to recover the secret.

- [`SecretSharingWrapper.combine` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-secret-sharing-wrapper/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.html#combine(java.util.List))
- [`SecretSharingWrapper.combine` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-secret-sharing-wrapper/-/blob/829bbe3b1a36598a33f076a8c146d0354d82109c/src/main/java/org/magmacollective/darkcrystal/secretsharingwrapper/SecretSharingWrapper.java#L131)

### Step 6 - Validate secret

![recovery successful](../assets/recover-success-sm.png)

The MAC is used to establish that recovery was successful.  This means we can be sure the combining process worked as planned and offers some protection against tampering.

- [`KeyBackupCrypto.secretUnbox` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L241)

### Step 7 - Decrypt secret

![secret and label](../assets/dc_secret_label.png)

Finally the secret is restored, along with a descriptive label.

- [`KeyBackupCrypto.decrypt` in API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-crypto-java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.html#decrypt(byte%5B%5D,byte%5B%5D))
- [`KeyBackupCrypto.decrypt` in source code](https://gitlab.com/dark-crystal-java/dark-crystal-key-backup-crypto-java/-/blob/70f663ea2520c1a7aeb03fd2c0d1092ebcc028ed/src/main/java/org/magmacollective/darkcrystal/keybackup/crypto/KeyBackupCrypto.java#L176)

### Step 8 - Recover original account

Depending on whether the lost account might have been compromised, it may be appropriate to abandon the new identity and continue to use the old one. If this is not the case, the key can at least be used to recover data encrypted to it.

In order to avoid confusion between the new 'temporary' identity, and the original one being restored, it may make sense, at the user-interface level, to restrict what a peer is able to with the application whilst they are in the process of retrieving shards from custodians.  For example making clear that they are currently in 'recovery mode' and are not able to use the normal features of the application until either their original identity is restored, or they create a 'normal' new account.

