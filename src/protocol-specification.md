# Dark Crystal Key Backup Protocol Specification


[introduction and terms](./protocol-specification/intro-terms.md)

[who is this for?](./protocol-specification/who-for.md)

[cryptographic primitives](./protocol-specification/crypto-primitives.md)

[higher-level cryptographic functions](./protocol-specification/higher-level-crypto-functions.md)

[shamir's secret sharing](./protocol-specification/shamirs-secret-sharing.md)

[anatomy of a shard](./protocol-specification/anatomy-of-a-shard.md)

[modules overview](./protocol-specification/modules-overview.md)

[set-up process](./protocol-specification/set-up-process.md)

[recovery process](./protocol-specification/recovery-process.md)
