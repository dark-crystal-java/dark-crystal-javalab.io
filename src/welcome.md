<a href="https://dark-crystal-java.gitlab.io/welcome.html">
<h2 style="text-align: center;">Distributed backup with Shamir's Secret Shares</h2>
</a>
<p align="center">
    <img src="./assets/friends.jpg" alt="crystal sharding image" width="600"/>
</p>


Dark Crystal transforms secrets into crystal shards that you can send to trusted friends. If you lose the secret, or something happens to you, your friends can combine the shards to recover the crystal and reveal the secret.

Dark Crystal uses cryptography so that no individual shard reveals anything about your secret on its own. Your secret is only revealed if the people you chose cooperate and put their shards together.
