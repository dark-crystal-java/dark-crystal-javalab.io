# Third iteration of Distributed backup - refining the design

With the third iteration, the changes we made began to get smaller, and we had the feeling we are almost ready to move from white-boarding to something more presentable. 

## Backup process from secret-owner's point of view

![backup1-secret-owner](https://gitlab.com/dark-crystal-java/briar-account-backup-design/-/raw/master/assets/iteration3-backup-secret-owner.jpg)

There is no longer an option to make a second backup with different custodians.

Viewing an existing backup also displays confirmations as to whether shards have been successfully received.  It is beyond the scope of the proof-of-concept to address the situation that some are never received. 

Potentially also displayed here is the date and time when a custodian was last online (a backup 'health check').

Updating the backup when more contacts are added happens automatically and requires no involvement from the peer.

Shards are sent as briar messages using the bramble protocol.

## Backup process from custodian's point of view

![backup2-custodian](https://gitlab.com/dark-crystal-java/briar-account-backup-design/-/raw/master/assets/iteration3-backup-custodian.jpg)

This remains unchanged from the previous iteration.  There is no mandatory consent-gaining from custodians in the proof-of-concept.

## Recovery process from secret-owner's point of view

![recovery-1-secret-owner](https://gitlab.com/dark-crystal-java/briar-account-backup-design/-/raw/master/assets/iteration3-recovery-secret-owner.jpg)

It is worth noting that aliases or public keys of custodians are never transmitted during recovery for security reasons, so cannot be displayed in the interface.

There is no longer a 'recover account' button displayed when the threshold is reached. The peer will immediately be prompted to create a password for their recovered account.  This is because until this is done, we are in the precarious situation of holding the secret unencrypted.

The option to receive a shard by remote link has also been removed. The proof-of-concept will only allow shards to be returned in-person.

## Recovery process from custodian's point of view

![recovery-2-custodian](https://gitlab.com/dark-crystal-java/briar-account-backup-design/-/raw/master/assets/iteration3-recovery-custodian.jpg)

Without the additional step of choosing either QR code or remote link, custodians now reach the QR code directly from the options menu for a particular contact.

This would be another reason why it is probably a good idea if the QR code does not contain the shard data itself, but a key used in the process of transferring it by other means, in case this option is reached accidentally.

So the proposal is that scanning the QR code initiates the process of transmitting the shard encrypted either by bluetooth, wifi, or over TOR, but importantly this transfer takes place quickly enough that confirmation is given whilst the custodian and secret owner are together - to avoid the situation that they need to re-meet if the transmission failed.  That is - it should not take longer than a couple of minutes.
