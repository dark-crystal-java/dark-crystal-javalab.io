## Understandable terminology in the UI

Following this session we made only minor changes to the mock-up user interface android app.

![Social backup menu item](../assets/socialbackup-settings-menu.png)

The feature is now called 'Social backup', not 'Distributed backup'.

![Trusted contacts](../assets/select-trusted-contacts.png)

Custodians are always referred to as 'trusted contacts'.

![Received backup piece](../assets/received-backup-piece.png)

For now, the term 'shard' is replaced by 'backup piece'. We may decide to add an extra explainer screen giving the analogy of breaking a crystal into 'shards' which are later put back together to recover the account, but without that, 'shard' is too confusing.

## Allowed threshold values

![Choose threshold](../assets/choose-threshold.png)

We now give feedback as to whether a chosen threshold value is recommended as 'secure'.  But some values are simply not allowed, and the slider in the UI will not permit those values to be chosen.  A threshold value of 100% (for example 5 of 5) is not allowed, nor is a value of 1, as this would be giving complete custody of the account to one person.

We may decide that choosing a threshold value is altogether too confusing and replace it with an automatically chosen proportion of the number of custodians, such as 75%.  But we will leave this option in initially in order to get feedback in testing sessions.

## Additional Confirmation Screens

![Confirmation](../assets/confirmation.png)

On trying out the app, we felt that more feedback was needed to let the peer know that certain processes had completed successfully, so we added some extra confirmation screens.
