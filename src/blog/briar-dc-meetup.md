# Briar and Dark Crystal Meetup - Introduction

![briar dark crystal](../assets/briar-dark-crystal.png)

We are collaborating with [Briar](https://briarproject.org/), a secure mobile messaging app, to develop a feature for 'distributed account backup' using Dark Crystal.

Unfortunately due to the situation with COVID19 we were not able to have the planned three-day meetup in person.

Instead we are having a series of remote meetings on Jitsi where we are collaboratively designing the feature.  Three members of our team met with Michael Rogers and Torsten Grote from Briar. 

## What is Briar?

![briar screenshots](../assets/briar-screenshots.gif)

Briar is a mobile messaging app designed for security. It relies on no servers and uses a custom-made protocol which works over bluetooth and wifi as well as over the internet using TOR.  Take a look at their ['how it works' page](https://briarproject.org/how-it-works/) for a high level overview, or [the wiki](https://code.briarproject.org/briar/briar/-/wikis/home) for technical details.

## What are we meeting for?

We already had some ideas about how this feature might look, based on previous discussion while we we writing the proposal for this project.  But its important to establish a shared understanding, as is always the case, each person carries their own assumptions about what we are trying to achieve.  So the purpose of meeting is to make sure these assumptions align before starting work.

Gaining agreement of the design details between five people can be a very time-consuming process, and sometimes it felt like we could really save time if somebody just went ahead and made these decisions.  But given the different roles we all have, whether it be in organising the testing sessions for the feature once it is built, writing documentation and reports, or back or front end development, being involved in the design process from the beginning creates an understanding necessary for genuine collaboration.

## 'Proof-of-concept'

We are trying to come up with a design for an initial iteration of development which will demonstrate that the proposed system works in the simplest possible way.  This is a 'proof-of-concept' or 'minimum viable product'.  When designing a feature, there are all sorts of interesting variations, special cases, or additional functionality one might want to add.  A large part of our time was spend deciding which of these were necessary for our 'proof-of-concept' design, and which should be shelved until we have something we can demonstrate in order to get additional feedback on. 

## Existing features of the Bramble protocol

Since Briar is a highly security-oriented application, a lot of the challenges we might normally face when designing a system of distributed social backups have already been addressed. For example, Briar's 'Bramble' protocol already includes handshaking and encryption which gives us forward-secrecy.

Perhaps this makes Briar a bad candidate to demonstrate the use of the Dark Crystal developer toolkit, since we are able to leave a lot of the heavy-lifting to the underlying protocol.  On the other hand, since Briar is designed for specifically for high-risk users, we are required to be extra-vigilant when it comes to security.

## Existing proposal for account backup

An account backup feature has been in the pipeline for Briar for some time, although not necessarily using social recovery.  Suppose you get a new phone, there is currently no way to transfer your briar account onto it.  The reason this has not yet been implemented is because of the complications that could be created if you use the same account from two devices.  This is a problem which will need addressing, albeit a problem unique to Briar.  For the proof-of-concept we are going to assume that the reason for wanting to recover the account is that access to the account has been lost.

## What are we backing-up?

When we talk about backing up the 'Briar account', we mean backing up the keys which make the account 'yours', meaning your contacts will be able to continue to contact you.

But there are other kinds of data stored by the Briar application which peers might not want to loose, for example their contacts list, their messages, and their membership of particular groups, forums and blogs. It should be noted that the actual content of forums or blogs themselves does not need to be backed up, since when existing content can be retrieved from other peers when re-joining. 

In deciding what to backup, we have to a trade-off of what information we are most concerned about loosing, against what information we are most concerned about an attacker having access to should our backup become compromised.

Our initial research showed high-risk users were most concerned about loosing their contacts list, as it can often be very impractical or dangerous to regain contact by other means.

Message content however, seems less appropriate to back up, as it is likely to contain sensitive details, and the other party of the conversation generally still has access to it. That is, re-gaining contact with someone gives you indirect access to your previous conversation with them, and all you really gain by including the messages in the backup is the convenience of having them displayed on your device without needing to ask.

Besides the clear disadvantage with regards to security, backing up content also has the disadvantage of being arbitrarily big.  Since we are asking others to hold the backup on their own devices, it becomes less appealing for them the bigger the backup gets.

So this feature will provide a backup of the cryptographic identity securing the account, and the contact list.
