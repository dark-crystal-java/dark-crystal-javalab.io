# Forth Iteration - finalising workflow

In this iteration we focussed less on what is going on on the back end, and more on making sure we are able to make the process understandable and intuitive.  Although it felt a bit early to be doing this, its important to establish that the model is not inherently confusing before building too much. 

## Backup process from secret-owner's point of view

![backup secret owner](/assets/iteration4-backup-secret-owner.png)

Possibly we also need to add an 'explainer screen' before choosing trusted contacts.

Also, we have not added a screen showing what happens if the shards are never received.  The thing to do here would be offer them to make a new backup and re-choose custodians.  Which we decided was out of scope of the proof of concept.  So for now they would just see no check-mark next to the custodian's name when viewing the existing backup.

## Backup process from custodian's point of view

![backup custodian](/assets/iteration4-backup-custodian.png)

## Recovery process from secret owner's point of view

![recovery secret owner](/assets/iteration4-recovery-secret-owner.png)

There is now an extra 'explainer' screen, and an 'error' screen for if the shard transfer fails.

## Recovery process from custodian's point of view

![Recovery custodian](/assets/iteration4-recovery-custodian.png)

Success / failure screens added.

Note that there is also currently no option for a custodian to delete the shard. Adding this might complicate the proof-of-concept but should be included in the feature.
