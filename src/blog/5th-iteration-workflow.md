# Fifth iteration of social backup feature - UI Mockup on Android

![android studio screenshot](/assets/iteration5-android-studio.png)

At this stage we begin a mock-up of the user interface. We considered using a UI design tool such as Figma for this, but eventually decided to make the design 'for real' using android studio, to avoid needing to convert the design, and mitigate encountering any android-specific issues later.

So we now have a fork of Briar with this feature present in the user interface.  But it is a mock-up, meaning nothing actually happens on the back end when these options are chosen.  This way we get a good impression of how the feature will look and are able to get feedback on it before implementing the back end component.

## Backup process - secret owner's point of view

![settings menu](/assets/iteration5-Screen04.png)

Here we can see the 'Distributed backup' option in Briar's settings menu.

![select custodians](/assets/iteration5-Screen05.png)

We select custodians from our contacts list using check boxes

![threshold slider](/assets/iteration5-Screen06.png)

The threshold is chosen using a slider, with an image giving a visual representation of the proportion of shards needed to recover.

![confirmation](/assets/iteration5-Screen07.png)

Confirmation that shards are sent.

## Recovery process - secret owner's point of view 

![](/assets/iteration5-Screen08.png)

On starting Briar for the first time we choose whether to recover an account or start fresh

![](/assets/iteration5-Screen09.png)

Explainer screen

![](/assets/iteration5-Screen10.png)

Error on receiving shard

![choose password](/assets/iteration5-Screen11.png)

On recovering the account, the peer is prompted to choose a new password.

## Recovery process - custodian's point of view

'Help recover account' option chosen from the options menu on the screen for the conversation with the secret owner.

![](/assets/iteration5-Screen12.png)

Explainer screen, before scanning QR code

![](/assets/iteration5-Screen13.png)

Error screen, on failed transmission

## Mutual QR code scan

We discussed whether to require a 'mutual' QR code scan, where each party both provides and receives a QR code, rather than a single QR code scan.  Requiring a mutual scan would make things harder for an attacker, but some Briar users have reported that they find the process of a two-way QR code scanning confusing (mutual scanning is used in exchanging contact details).

If we would only require a single scan, we discussed which party would provide the code. Imagine an attacker had a hidden device at the site where the QR code exchange took place.

Lets suppose the custodian provides the code.  If the attacker is able to capture the QR code with a camera, and connect electronically faster than the secret-owner, they could potentially intercept the shard.

But if the secret-owner provides the code, the attacker has the opportunity to provide a 'false' shard, sabotaging the recovery process. But they gain no information, and the recovery can still take place later.  So if only one party would provide a code, it should be the secret owner. If we want to improve security at the cost of a more confusing user experience, a mutual QR code scan should take place.

## Terminology discussion

- We discussed whether to use 'Distributed backup' - technical language perhaps difficult to understand but accurately describes the feature. 'Social backup' is perhaps better. The subtext of the menu item is 'Backup your account using trusted contacts'
- We decided 'Trusted contacts' should always be used, rather than 'custodians'
- 'Shard' should only be used if there is an explainer with the analogy of breaking crystal into shards which can later be recombined. This is perhaps difficult when designing the interface from the custodians point of view, as there is less opportunity to give them an explainer screen when they 'passively' receive a shard.  Perhaps in this case 'backup piece' is better, eg: 'Alice has sent you a piece of her account backup'.
