# 6th iteration - Back-end flowcharts

## Refining the back-end design

In the UI mock-ups, we have focussed on two processes, the process of backing up the account, and the process of recovering one.  But there will actually be a third process involved in this feature - updating the backup when the contacts list is modified.

This happens automatically with no involvement from the user, which is why there is nothing to show in the UI mock-ups. But on the back end we needed to design exactly how this would work.

### Backup Creation - Secret Owner

![Backup - secret owner](../assets/flowcharts/backup-creation-owner.png)

As with normal Briar messages backup metadata and shard messages are stored in the account database, waiting to be 'synced', as Briar uses an offline-first protocol.

### Backup Creation - Custodian

![Backup - custodian](../assets/flowcharts/backup-creation-custodian.png)

When a custodian receives a shard, if they have already backed-up their own account, they add the shard to their own backup, and update their backup the same way they do when they modify their contacts list (described below). This gives additional redundancy - when you restore your account, you also restore the shards you were holding of the accounts of others.  This is an additional layer of mutual inter-dependence.

### Backup Update - Secret Owner

![Update - secret owner](../assets/flowcharts/backup-update-owner.png)

Each custodian currently holds a shard of a encryption key, and a backup of the secret-owner's account encrypted with this key. So when the backup is updated, no new shard need to be sent, only a new version of the encrypted backup.  This is possible because the secret-owner retains a local copy of this encryption key in their encrypted Briar database.

### Backup Update - Custodian

![Update - custodian](../assets/flowcharts/backup-update-custodian.png)

For the proof-of-concept design, each update contains a complete copy of the backup, which replaces the previous version.  Later versions might involve 'incremental' backups - where only the changes between the current version and the previous one are transferred with each update, similar to how version control works.

However at this stage, complete backups are not costly to transmit as they are likely to be small.  If deciding to expand the account backup feature to also backup content, we would reconsider this decision.

### Account Recovery - Secret Owner

![Recovery - secret owner](../assets/flowcharts/account-restoration-owner.png)

This is by far the most complicated part to implement, as it involves many components which don't currently exist in Briar, such as a temporary database for storing the collected shards.

Shards are transferred over a temporary network connection, initiated by the scanning of a QR code in-person.

The QR code includes a key fingerprint that's used to authenticate the temporary connection, so both parties can be sure they've connected to the person whose QR code they scanned.

### Account Recovery - Custodian

![Recovery - custodian](../assets/flowcharts/account-restoration-custodian.png)

The custodian chooses the owner from their contact list before sending the shard as described above.
