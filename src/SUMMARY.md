# Summary

- [Welcome!](./welcome.md)
    - [why dark crystal?](./why-dark-crystal.md)
    - [what is secret sharing?](./what-is-secret-sharing.md)
- [Protocol Specification](./protocol-specification.md)
    - [introduction and terms](./protocol-specification/intro-terms.md)
    - [who is this for?](./protocol-specification/who-for.md)
    - [cryptographic primitives](./protocol-specification/crypto-primitives.md)
    - [higher-level crypto functions](./protocol-specification/higher-level-crypto-functions.md)
    - [shamir's secret sharing](./protocol-specification/shamirs-secret-sharing.md)
    - [anatomy of a shard](./protocol-specification/anatomy-of-a-shard.md)
    - [modules overview](./protocol-specification/modules-overview.md)
    - [back-up process](./protocol-specification/set-up-process.md)
    - [recovery process](./protocol-specification/recovery-process.md)
- [Choosing an SSS implementation](./choice-of-sss-implementation.md)
    - [introduction](./choosing-an-sss-implementation/introduction.md)
    - [requirements](./choosing-an-sss-implementation/requirements.md)
    - [a standard for sss](./choosing-an-sss-implementation/standard.md)
    - [implementations considered](./choosing-an-sss-implementation/implementations.md)
    - [bindings to java](./choosing-an-sss-implementation/bindings-java.md)
    - [conclusion](./choosing-an-sss-implementation/conclusion.md)
    - [references](./choosing-an-sss-implementation/references.md)
- [Worked example](./worked-example.md)
- [Security Considerations](./security-considerations.md)
    - [guide to social contexts](./guide-to-social-contexts.md)
    - [threat-modelling the protocol](./threat-modelling-dark-crystal.md)
- [Dark Crystal in Java API Docs](./dark-crystal-java-api-documentation.md)
- [Blog - Designing a social backup feature for Briar](./blog.md)
    - [Briar and Dark Crystal Meetup - Introduction](./blog/briar-dc-meetup.md)
    - [1st Iteration - Initial White-boarding](./blog/first-iteration-workflow.md)
    - [2nd Iteration - Improving workflow](./blog/Second-iteration-of-briar-distributed-account-backup-workflow.md)
    - [3rd Iteration - Refining the design](./blog/third-iteration-workflow.md)
    - [4th Iteration - Finalising workflow](./blog/4th-iteration-workflow.md)
    - [5th Iteration - UI mock-up on Android](./blog/5th-iteration-workflow.md)
    - [6th Iteration - Back-end flowcharts](./blog/6th-iteration-workflow.md)
    - [7th Iteration - UI Terminology](./blog/7th-iteration-workflow.md)
